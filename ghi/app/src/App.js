import './App.css';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import AttendConferenceForm from './AttendConferenceForm';
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
            <Route path='locations'>
              <Route path="new" element={<LocationForm/>}/>
            </Route>
            <Route path='attendees' element={<AttendeesList/>}>
              <Route path="new" element={<LocationForm/>}/>
            </Route>
            <Route path='locations'>
              <Route path="new" element={<LocationForm/>}/>
            </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
