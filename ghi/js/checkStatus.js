const payloadCookie = Promise.resolve(cookieStore.get("jwt_access_payload"))
if (payloadCookie) {
    payloadCookie.then(result => {
        const payload = JSON.parse(atob(result.value))
        if ("events.add_location" in payload.user.perms) {
            document.getElementById("new-location-button").classList.remove("d-none")
        }
    })
} else {
    console.log('jwt_access_payload not found')
}
